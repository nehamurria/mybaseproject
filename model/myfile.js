const myfileSchema = require("schema/mysql/myfile");
const myfileTable = myfileSchema.tableName;
const myfileFieldmap = myfileSchema.fieldMap;
const config = require("config");
const commonFunctions = require("Utils/CommonFunctions");
const mySqlConnection = config.mySqlConnection;
const mysql = require("mysql");

const save = function (myfileInfoObject) {
    'use strict';
    //

    let insertionQuery = `INSERT INTO \`${myfileTable}\` `;
    let fieldsToInsert = [];
    let inserts = [];
    let placeHolders = [];
    fieldsToInsert.push("`" + myfileFieldmap.id + "`");
//    fieldsToInsert.push("`" + adminFieldmap.intId + "`");
//    placeHolders.push("?");
//    inserts.push("NULL");
    //
    if (myfileInfoObject.email) {
        fieldsToInsert.push("`" + myfileFieldmap.email + "`");
        placeHolders.push("?");
        inserts.push(myfileInfoObject.email);
    }
    if (myfileInfoObject.userName) {
        fieldsToInsert.push("`" + myfileFieldmap.userName + "`");
        placeHolders.push("?");
        inserts.push(myfileInfoObject.userName);
    }
    if (myfileInfoObject.password) {
        fieldsToInsert.push("`" + myfileFieldmap.passwordHash + "`");
        placeHolders.push("?");
        inserts.push(commonFunctions.hashPasswordUsingBcrypt(myfileInfoObject.password));
    }
    if (myfileInfoObject.mobileNumber) {
        fieldsToInsert.push("`" + myfileFieldmap.mobileNumber + "`");
        placeHolders.push("?");
        inserts.push(myfileInfoObject.mobileNumber);
    }


let fieldsSpecificationString = "(" + fieldsToInsert.join(",") + ")";
    insertionQuery = insertionQuery + fieldsSpecificationString + " VALUES(ordered_uuid(uuid())," + placeHolders.join(",") + ") ";
    let deferred = Q.defer();
    let mQuery = mysql.format(insertionQuery, inserts);
};
module.exports = {
    save: save
};
