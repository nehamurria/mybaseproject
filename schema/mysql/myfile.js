'use strict';
const config = require("config");
const mySqlConnection = config.mySqlConnection;
const tableName = 'myfile';
const collate = "utf8_unicode_ci";
const charset = "utf8";
const engine = "InnoDB";

const fieldMap = {
    id: "id",
    email: "email",
    userName: "userName",
    passwordHash: "passwordHash",
    mobileNumber: "mobileNumber"

};
const createTable = `CREATE TABLE IF NOT EXISTS ${tableName} (
    ${fieldMap.id} binary(16) NOT NULL PRIMARY KEY,
    ${fieldMap.email} VARCHAR(200) NOT NULL,
    ${fieldMap.userName} VARCHAR(200) DEFAULT NULL,
    ${fieldMap.passwordHash} CHAR(60) DEFAULT NULL,
    ${fieldMap.mobileNumber} binary(16) NOT NULL,
    CONSTRAINT ${tableName}_userName_unq UNIQUE (${fieldMap.userName})
    ) ENGINE=${engine} CHARACTER SET ${charset} COLLATE ${collate};`;

    
const prep = function () {
    if (process.env.MUST_DEBUG == 'true') {
        console.log('Success');
}
}


if (process.env['PREPARE_TABLES'] == 'true') {
    prep();
}
module.exports = {
    tableName: tableName,
    fieldMap: fieldMap
};
